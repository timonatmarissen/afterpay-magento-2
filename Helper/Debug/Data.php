<?php
/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */

namespace Afterpay\Payment\Helper\Debug;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\DataObject;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\Session\Storage as Session;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Filesystem\Io\File;

/**
 * AfterPay Debug helper
 */
class Data extends AbstractHelper
{
    /**
     * Security message for masked values
     */
    const SECURITY_MESSAGE = 'REMOVED FOR SECURITY REASONS';

    /**
     * Fields that should be replaced in debug with security message
     *
     * @var array
     */
    protected $debugReplacePrivateDataKeys = ['password'];

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * Logging instance
     *
     * @var Monolog
     */
    protected $logger;

    /**
     * Log file folder path
     *
     * @var string
     */
    protected $folderPath = '/var/log/afterpay/';

    /**
     * Log file folder path
     *
     * @var string
     */
    protected $filePath;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Encryption interface
     *
     * @var EncryptorInterface
     */
    protected $encryption;

    /**
     * @var File
     */
    protected $file;

    /**
     * Log file extension
     *
     * @var string
     */
    protected $fileExtension = '.txt';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $paymentMethodCode = '';
    /**
     * @var DataObject
     */
    protected $postObject;

    /**
     * @param Context $context
     * @param AttachmentTransportBuilder $attachmentTransportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param Escaper $escaper
     * @param Monolog $logger
     * @param Session $session
     * @param EncryptorInterface $encryption
     * @param File $file
     * @param DataObject $postObject
     *
     * @throws \Exception
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        Escaper $escaper,
        Monolog $logger,
        Session $session,
        EncryptorInterface $encryption,
        File $file,
        DataObject $postObject
    ) {
    
        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager;
        $this->escaper = $escaper;
        $this->logger = $logger;
        $this->session = $session;
        $this->encryption = $encryption;
        $this->file = $file;
        $this->postObject = $postObject;
        // Create folder for log files
        // @codingStandardsIgnoreStart
        $this->setupLogging();
        // @codingStandardsIgnoreEnd
    }

    /**
     * Debug data
     *
     * @param string $paymentMethod
     * @param $data
     * @param bool $sendmail
     */
    public function debug($paymentMethod, $data, $sendmail = false)
    {
        $this->paymentMethodCode = $paymentMethod;
        $this->_isDebugEnabled() ? $this->_processDebug($data, $sendmail) : false;
    }

    /**
     * Debug enabled
     *
     * @return mixed
     */
    protected function _isDebugEnabled()
    {
        return $this->scopeConfig->getValue($this->_getXmlPathDebugEnabled());
    }

    /**
     * Send email enabled
     *
     * @return mixed
     */
    protected function _isEmailEnabled()
    {
        return $this->scopeConfig->getValue($this->_getXmlPathDebugEmailEnabled());
    }

    /**
     * Debug data using Zend library
     *
     * @param $data
     * @param $sendmail
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _processDebug($data, $sendmail)
    {
        $this->logger->debug(json_encode($data));

        if ($sendmail) {
            $this->_sendDebugMail($data);
        }
    }

    /**
     * Filter data for senstive debug info
     *
     * @param $data
     *
     * @return mixed
     */
    protected function _filterData($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (in_array($key, $this->_getDebugReplacePrivateDataKeys())) {
                    $data[$key] = self::SECURITY_MESSAGE;
                }

                if (is_object($value)) {
                    $this->_filterObject($value);
                } elseif (is_array($value)) {
                    $this->_filterData($value);
                }
            }
        }

        return $data;
    }

    /**
     * Filter debug data object
     *
     * @param $data
     *
     * @return mixed
     */
    protected function _filterObject($data)
    {
        if (is_object($data)) {
            foreach ($data as $key => $value) {
                if (in_array($key, $this->_getDebugReplacePrivateDataKeys())) {
                    $data->$key = self::SECURITY_MESSAGE;
                }

                if (is_object($value)) {
                    $this->_filterObject($value);
                } elseif (is_array($value)) {
                    $this->_filterData($value);
                }
            }
        }

        return $data;
    }

    /**
     * Send an email with debug file attached
     *
     * @param $data
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _sendDebugMail($data)
    {
        $recipient = explode(
            ',',
            $this->scopeConfig->getValue($this->_getXmlPathDebugRecipientEmail())
        );

        if ($recipient && $this->_isEmailEnabled()) {
            // Store base url
            $storeBaseUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB);
            $storeBaseUrl = parse_url($storeBaseUrl); // @codingStandardsIgnoreLine
            $storeBaseUrl = $storeBaseUrl['host'];

            $this->postObject->setData([
                "store_url" => $storeBaseUrl,
                'debug' => json_encode($data, JSON_PRETTY_PRINT)
            ]);

            $transport = $this->transportBuilder
                ->setTemplateIdentifier('debug_email_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['data' => $this->postObject])
                ->addTo($recipient)
                ->getTransport();

            try {
                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->debug($this->paymentMethodCode, $e);
            }
        }
    }

    /**
     * Get hashed customer session id
     *
     * @return string
     */
    protected function _getHashedSessionId()
    {
        $visitorSession = $this->session->getVisitorData();
        $visitorSessionId = $visitorSession['session_id'];

        return $this->encryption->hash($visitorSessionId);
    }

    /**
     * Get full log file path
     *
     * @param $filename
     *
     * @return string
     */
    protected function _getFilePath()
    {
        return $this->folderPath . $this->_getHashedSessionId();
    }

    /**
     * Creates log folder
     *
     * @throws \Exception
     */
    protected function setupLogging()
    {
        // Create folder for log files
        $this->folderPath = BP . $this->folderPath;
        $this->file->checkAndCreateFolder($this->folderPath);
        $this->filePath = $this->_getFilePath() . $this->fileExtension;
        $this->logger->pushHandler(new \Monolog\Handler\StreamHandler($this->filePath));
    }

    /**
     * Return replace keys for debug data
     *
     * @return array
     */
    protected function _getDebugReplacePrivateDataKeys()
    {
        return (array)$this->debugReplacePrivateDataKeys;
    }

    /**
     * Debug enabled config path
     *
     * @return string
     */
    protected function _getXmlPathDebugEnabled()
    {
        return 'payment/' . $this->paymentMethodCode . '/debug';
    }

    /**
     * Email enabled config path
     *
     * @return string
     */
    protected function _getXmlPathDebugEmailEnabled()
    {
        return 'payment/' . $this->paymentMethodCode . '/debug_email';
    }

    /**
     * Recipient email config path
     *
     * @return string
     */
    protected function _getXmlPathDebugRecipientEmail()
    {
        return 'payment/' . $this->paymentMethodCode . '/debug_email_address';
    }
}
