<?php
/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */
namespace Afterpay\Payment\Model\Config\Provider\Base;

use Afterpay\Payment\Helper\Service\Data;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use Psr\Log\LoggerInterface;
use Magento\Payment\Gateway\Config\Config;

/**
 * Payment method configuration provider
 */
class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'afterpay_base';

    /**
     * @var Repository
     */
    protected $assetRepo;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var \Afterpay\Payment\Gateway\Config\Config
     */
    protected $config;

    /**
     * @var null|string
     */
    protected $code;

    /**
     * @param Config $config
     * @param Repository $assetRepo
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param LoggerInterface $logger
     * @param string|null $code
     */
    public function __construct(
        Config $config,
        Repository $assetRepo,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        LoggerInterface $logger,
        string $code = null
    ) {
    
        $this->assetRepo = $assetRepo;
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        $this->logger = $logger;
        $this->config = $config;
        $this->code = $code;
    }
    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                $this->code => [
                    'code' => $this->code,
                    'payment_icon' => Data::AFTERPAY_SVG_ICON,
                    'title' => $this->config->getValue('title'),
                    'description' => $this->config->getValue('description'),
                    'allowed_countries' => $this->config->getValue('specificcountry'),
                    'success' => $this->config->getValue('success'),
                    'testmode' => $this->config->getValue('testmode'),
                    'testmode_label' => Data::TEST_MODE_LABEL,
                    'can_coc' => $this->config->getValue('coc'),
                    'can_bankaccount' => $this->config->getValue('bankaccount'),
                    'can_dob' => $this->config->getValue('dob'),
                    'can_company_name' => $this->config->getValue('company_name'),
                    'can_ssn' => $this->config->getValue('ssn'),
                    'terms_and_conditions' => $this->config->getValue('terms_and_conditions'),
                    'can_privacy' => $this->config->getValue('privacy'),
                    'can_gender' => $this->config->getValue('gender'),
                    'can_number' => $this->config->getValue('number'),
                    'can_bank_code' => $this->config->getValue('bank_code'),
                ]
            ],
        ];
    }
}
