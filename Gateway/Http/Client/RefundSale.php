<?php
/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\Afterpay;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;

class RefundSale extends AbstractTransaction
{
    const ORDER_MANAGEMENT_CODE = 'OM';

    /**
     * Service model
     */
    protected $afterpayService;

    /**
     * Debug
     *
     * @var
     */
    protected $debugHelper;

    /**
     * @param LoggerInterface $logger
     * @param Afterpay $afterpayService
     * @param Logger $customLogger
     * @param DebugHelper $debugHelper
     */
    public function __construct(
        LoggerInterface $logger,
        Afterpay $afterpayService,
        Logger $customLogger,
        DebugHelper $debugHelper
    ) {
        parent::__construct($logger, $customLogger, $afterpayService);
        $this->afterpayService = $afterpayService;
        $this->debugHelper = $debugHelper;
    }

    /**
     * @inheritdoc
     */
    public function execute(array $data, array $clientConfig)
    {
        if ($this->isRestOrder($clientConfig)) {
            $this->afterpayService->setRest();
        }

        $this->afterpayService->setOrderCountry($data['order_country']);
        $this->afterpayService->set_ordermanagement($data['refund_type']);
        unset($data['order_country']);

        foreach ($data['orderlines'] as $line) {
            $this->afterpayService->create_order_line(...array_values($line));
        }

        # Need to set refund type AFTER lines
        $this->afterpayService->set_order($data, self::ORDER_MANAGEMENT_CODE);

        $this->afterpayService->do_request(
            $clientConfig,
            $clientConfig['modus']
        );

        return $this->afterpayService->order_result->return;
    }
}
