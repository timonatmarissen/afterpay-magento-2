<?php
/**
 * Copyright (c) 2018  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Http;

use Magento\Config\Model\Config\Backend\Encrypted;
use Magento\Payment\Gateway\Config\ValueHandlerInterface;
use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;

class TransferFactory implements TransferFactoryInterface
{
    /**
     * @var TransferBuilder
     */
    private $transferBuilder;

    /**
     * @var ValueHandlerInterface
     */
    private $configValueHandler;

    /**
     * @var Encrypted
     */
    private $encrypted;

    /**
     * @param Encrypted $encrypted
     * @param \Magento\Payment\Gateway\Http\TransferBuilder $transferBuilder
     * @param ValueHandlerInterface $configValueHandler
     */
    public function __construct(
        Encrypted $encrypted,
        TransferBuilder $transferBuilder,
        ValueHandlerInterface $configValueHandler
    ) {
        $this->transferBuilder = $transferBuilder;
        $this->configValueHandler = $configValueHandler;
        $this->encrypted = $encrypted;
    }

    /**
     * @inheritdoc
     */
    public function create(array $request)
    {
        return $this->transferBuilder
            ->setBody($request)
            ->setClientConfig($this->loadClientConfig())
            ->build();
    }

    /**
     * Handle weird/complicated mapping between field and mode names and return auth credentials from config
     *
     * @return array
     */
    private function loadClientConfig()
    {
        $modePrefix = ((bool) $this->configValueHandler->handle(['field' => 'testmode'])) ? 'testmode' : 'production' ;
        $fieldsMap = [
            'merchantid' => '_merchant_id',
            'portfolioid' => '_portfolio_id',
            'apiKey' => '_api_key'
        ];

        $result = [
            'modus' => $modePrefix === 'testmode' ? 'test' : 'live',
            'password' => $this->loadPassword($modePrefix),
            'has_rest' => (bool) $this->configValueHandler->handle(['field' => 'has_rest'])
        ];

        foreach ($fieldsMap as $key => $value) {
            $result[$key] = $this->configValueHandler->handle(['field' => $modePrefix . $value]);
        }

        return $result;
    }

    /**
     * @param $modePrefix
     * @return string
     */
    private function loadPassword($modePrefix)
    {
        return $this->encrypted->processValue(
            $this->configValueHandler->handle(['field' => $modePrefix . '_password'])
        );
    }
}
