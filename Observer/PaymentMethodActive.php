<?php
/**
 * Copyright (c) 2018  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Model\Config\Visitor;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;

class PaymentMethodActive implements ObserverInterface
{
    /**
     * @var Visitor
     */
    private $visitor;

    /**
     * @param Visitor $visitor
     */
    public function __construct(Visitor $visitor)
    {
        $this->visitor = $visitor;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        /* @var MethodInterface $methodInstance */
        $methodInstance = $event->getData('method_instance');
        /* @var Quote $quote */
        $quote = $event->getData('quote');
        $result = $event->getData('result');

        if ($this->shouldSkip($quote, $result, $methodInstance)) {
            return;
        }

        $available = $this->allowedForGroup($methodInstance, $quote)
            && $this->allowedIp($methodInstance)
            && $this->allowedShippingMethod($methodInstance, $quote);
        $result->setData('is_available', $available);
    }

    /**
     * @param Quote|null $quote
     * @param DataObject $result
     * @param MethodInterface $methodInstance
     * @return bool
     */
    private function shouldSkip($quote, $result, $methodInstance)
    {
        return $quote === null
            || $result->getData('is_available') === false
            || strpos($methodInstance->getCode(), 'afterpay') !== 0; // does not start with
    }

    /**
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     * @return bool
     */
    private function allowedForGroup($methodInstance, $quote)
    {
        if ($methodInstance->getConfigData('allowspecificgroup')) {
            $allowedGroups = explode(',', $methodInstance->getConfigData('specificgroup'));

            return in_array($quote->getCustomerGroupId(), $allowedGroups, true);
        }

        return true;
    }

    /**
     * @param MethodInterface $methodInstance
     * @return bool
     */
    private function allowedIp($methodInstance)
    {
        if ($methodInstance->getConfigData('restrict')) {
            return $this->visitor->allowedByIp();
        }

        return true;
    }

    /**
     * Is allowed payment for shipping method or not
     *
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     * @return bool
     */
    private function allowedShippingMethod($methodInstance, $quote)
    {
        $config = $methodInstance->getConfigData('excludeships');
        if ($config === null) {
            return true;
        }
        $methods = explode(',', $config);

        return !in_array($quote->getShippingAddress()->getShippingMethod(), $methods, true);
    }
}
