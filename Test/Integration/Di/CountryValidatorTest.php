<?php

namespace Afterpay\Payment\Test\Integration\Di;

use Magento\Framework\App\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Some basic tests for DI config
 */
class CountryValidatorTest extends TestCase
{
    public function facades()
    {
        return [
            ['DigitalInvoiceNLFacade', 'SK'],
            ['B2BNLFacade', 'GB'],
            ['DigitalInvoiceBEFacade', 'LT'],
            ['DigitalInvoiceNLDebitFacade', 'PL'],
            ['DigitalInvoiceDEFacade', 'AF']
        ];
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_nl_business_2_business/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_be_digital_invoice/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_de_invoice/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_nl_direct_debit/allowspecific 1

     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/specificcountry SK
     * @magentoConfigFixture default_store payment/afterpay_nl_business_2_business/specificcountry GB
     * @magentoConfigFixture default_store payment/afterpay_be_digital_invoice/specificcountry LT
     * @magentoConfigFixture default_store payment/afterpay_nl_direct_debit/specificcountry PL
     * @magentoConfigFixture default_store payment/afterpay_de_invoice/specificcountry AF
     *
     * @dataProvider facades
     */
    public function testAllowedInCountry($facadeClassName, $country)
    {
        $objectManager = ObjectManager::getInstance();
        $facade = $objectManager->get($facadeClassName);

        $this->assertEquals(
            true,
            $facade->canUseForCountry($country),
            sprintf('Test allowed country fail for %s', $facadeClassName)
        );
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_nl_business_2_business/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_be_digital_invoice/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_de_invoice/allowspecific 1
     * @magentoConfigFixture default_store payment/afterpay_nl_direct_debit/allowspecific 1

     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/specificcountry PL
     * @magentoConfigFixture default_store payment/afterpay_nl_business_2_business/specificcountry AF
     * @magentoConfigFixture default_store payment/afterpay_be_digital_invoice/specificcountry GB
     * @magentoConfigFixture default_store payment/afterpay_nl_direct_debit/specificcountry LT
     * @magentoConfigFixture default_store payment/afterpay_de_invoice/specificcountry SK
     *
     * @dataProvider facades
     */
    public function testNotAllowedInCountry($facadeClassName, $country)
    {
        $objectManager = ObjectManager::getInstance();
        $facade = $objectManager->get($facadeClassName);

        $this->assertEquals(
            false,
            $facade->canUseForCountry($country),
            sprintf('Test disallowed country fail for %s', $facadeClassName)
        );
    }
}
