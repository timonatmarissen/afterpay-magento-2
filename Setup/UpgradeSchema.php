<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Afterpay\Payment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 *
 * @package Afterpay\Payment\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var string
     */
    private static $connectionNameSales = 'sales';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $this->addNewColumns($setup);
        }
        if (version_compare($context->getVersion(), '2.0.7', '<')) {
            $this->addAfterpayProductImage($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addNewColumns(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('customer_entity'),
            'cocnumber',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 255,
                'nullable' => true,
                'default' => null,
                'comment' => 'Customer CoC number'
            ]
        );

        $setup->getConnection(self::$connectionNameSales)->addColumn(
            $setup->getTable('sales_order'),
            'afterpay_captured',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                'nullable' => false,
                'default' => '0',
                'comment' => 'Order captured in Afterpay'
            ]
        );
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addAfterpayProductImage(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_item'),
            'afterpay_product_image',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 1024,
                'nullable' => true,
                'default' => null,
                'comment' => 'Cart Item Image used for Afterpay'
            ]
        );
    }
}
