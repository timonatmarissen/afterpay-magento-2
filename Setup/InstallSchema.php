<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Afterpay\Payment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Add AfterPay fee columns
 */
class InstallSchema implements InstallSchemaInterface
{

    private static $connectionNameSales = 'sales';
    private static $connectionNameQuote = 'checkout';
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) // @codingStandardsIgnoreLine
    {
        $installer = $setup;
        $connectionSales = $installer->getConnection(self::$connectionNameSales);
        $connectionQuote = $installer->getConnection(self::$connectionNameQuote);
        $tablesSales = [
            'sales_order',
            'sales_invoice',
            'sales_order_payment'
        ];
        $tablesQuote = [
            'quote'
        ];

        foreach ($tablesSales as $table) {
            $connectionSales->addColumn(
                $installer->getTable($table),
                'afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee value'
                ]
            );

            $connectionSales->addColumn(
                $installer->getTable($table),
                'base_afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee base value'
                ]
            );
        }

        foreach ($tablesQuote as $table) {
            $connectionQuote->addColumn(
                $installer->getTable($table),
                'afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee value'
                ]
            );

            $connectionQuote->addColumn(
                $installer->getTable($table),
                'base_afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee base value'
                ]
            );
        }
    }
}
